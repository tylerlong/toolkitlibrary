﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace ToolkitLibrary.Http
{
    /// <summary>
    /// Http客户端
    /// </summary>
    public static class HttpClient
    {
        /// <summary>
        /// 下载二进制的资源, 保存到指定的硬盘路径上
        /// </summary>
        /// <param name="url">网络资源地址</param>
        /// <param name="filePath">保存的路径</param>
        public static bool DownloadBinary(string url, string filePath)
        {
            var request = WebRequest.Create(url);
            using (var rs = request.GetResponse().GetResponseStream())
            {
                var bufferSize = 1024 * 1024;
                var buffer = new byte[bufferSize];
                var length = 0;
                using (var fs = new FileStream(filePath, FileMode.Create))
                {
                    while ((length = rs.Read(buffer, 0, bufferSize)) > 0)
                    {
                        fs.Write(buffer, 0, length);
                    }
                }
            }
            return true;
        }
    }
}