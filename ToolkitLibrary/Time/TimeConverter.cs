﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToolkitLibrary.Time
{
    /// <summary>
    /// 跟时间转换有关的帮助方法
    /// </summary>
    public static class TimeConverter
    {
        /// <summary>
        /// 对时间进行向下取整, 取整的粒度可以随意指定
        /// 比如: 获取当前时间, 精确到小时就可以了
        /// 原理: 把时间转成整数, 然后按照整数来取整. 
        /// 时间转整数只要获取时间的Ticks就行了, 至于整数如何取整, 就是个纯粹的数学问题了.
        /// </summary>
        /// <param name="date">被取整的时间</param>
        /// <param name="span">取整的粒度</param>
        /// <returns>取整的结果</returns>
        public static DateTime Floor(this DateTime date, TimeSpan span)
        {
            //先做除法, 会舍去小数点的部分, 然后再做乘法, 最终起到了取整的效果
            return new DateTime(date.Ticks / span.Ticks * span.Ticks);
        }


        /// <summary>
        /// 对时间进行取整(中间值的情况向上取整), 取整的粒度可以随意指定
        /// 原理: 同Floor
        /// </summary>
        /// <param name="date">被取整的时间</param>
        /// <param name="span">取整的粒度</param>
        /// <returns>取整的结果</returns>
        public static DateTime Round(this DateTime date, TimeSpan span)
        {
            return new DateTime((date.Ticks + span.Ticks / 2 + 1) / span.Ticks * span.Ticks);
        }


        /// <summary>
        /// 对时间进行向上取整, 取整的粒度可以随意指定
        /// 原理: 同Floor
        /// </summary>
        /// <param name="date">被取整的时间</param>
        /// <param name="span">取整的粒度</param>
        /// <returns>取整的结果</returns>
        public static DateTime Ceil(this DateTime date, TimeSpan span)
        {
            return new DateTime((date.Ticks + span.Ticks - 1) / span.Ticks * span.Ticks);
        }


        /// <summary>
        /// 把指定时间转成unix时间, 就是从1970年1月1日零点到指定的时间共有多少秒
        /// </summary>
        /// <param name="datetime">时间</param>
        /// <returns>时间距离1970年1月1日有多少秒</returns>
        public static double ToUnixTime(this DateTime datetime)
        {
            return (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
        }
    }
}