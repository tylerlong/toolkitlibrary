﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace ToolkitLibrary.Exceptions
{
    /// <summary>
    /// TODO: 改进成一个灵活的log打印工具, 可以新建一个对象, 
    /// 然后给这个对象添加几个输出途径, 并且可以指定输出的异常的严重程度
    /// 高于指定的严重程度的就会输出.
    /// </summary>
    public class ExceptionPrinter
    {
        /// <summary>
        /// 打印异常信息
        /// </summary>
        /// <param name="e">异常</param>
        public static void Print(Exception e)
        {
            while (e.InnerException != null)
                e = e.InnerException;
            System.Diagnostics.Debug.Print("{0}\n{1}", e.Message, e.StackTrace);
        }
    }
}
