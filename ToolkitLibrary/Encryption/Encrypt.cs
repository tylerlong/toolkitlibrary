﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToolkitLibrary.Encryption
{
    /// <summary>
    /// class handling encryption
    /// </summary>
    public static class Encrypt
    {
        //why not new a Random instance each time GenerateRandomString() is called?
        //because GenerateRandomString may return the same values when called in quick succession.
        private static Random random = new Random((int)DateTime.Now.Ticks);

        /// <summary>
        /// Generate a random string
        /// </summary>
        /// <param name="length">the length of the string</param>
        /// <returns>the random string generated</returns>
        public static string GenerateRandomString(int length = 64)
        {
            var bytes = new byte[length / 2 + 1];
            random.NextBytes(bytes);
            var result = BitConverter.ToString(bytes).Replace("-", "").ToLower();
            result = result.Substring(0, length);
            return result;
        }
    }
}