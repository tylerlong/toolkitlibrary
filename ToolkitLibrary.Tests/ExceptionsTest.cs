﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ToolkitLibrary.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace ToolkitLibrary.Tests
{
    [TestClass]
    public class ExceptionsTest
    {
        [TestMethod]
        public void Print()
        {
            try
            {
                throw new Exception("这是一个故意抛出的异常");
            }
            catch (Exception e)
            {
                ExceptionPrinter.Print(e);
                Console.ReadLine();
            }
        }
    }
}
