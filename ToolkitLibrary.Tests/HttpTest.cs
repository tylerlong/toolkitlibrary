﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ToolkitLibrary.Http;

namespace ToolkitLibrary.Tests
{
    [TestClass]
    public class HttpTest
    {
        [TestMethod]
        public void DownloadBinary()
        {
            var url = "http://www.baidu.com/img/baidu_sylogo1.gif";
            var filePath = "D:\\baidu.gif"; 
            var result = HttpClient.DownloadBinary(url, filePath);
            Assert.IsTrue(System.IO.File.Exists(filePath));
            System.IO.File.Delete(filePath);
        }
    }
}