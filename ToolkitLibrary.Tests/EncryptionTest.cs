﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ToolkitLibrary.Encryption;
using System.Text.RegularExpressions;

namespace ToolkitLibrary.Tests
{
    [TestClass]
    public class EncryptionTest
    {
        [TestMethod]
        public void GenerateRandomString()
        {
            var str = Encrypt.GenerateRandomString(64);
            Assert.AreEqual(str.Length, 64);
            StringAssert.Matches(str, new Regex("^[0-9a-f]+$"));
            var str2 = Encrypt.GenerateRandomString(64);
            Assert.AreNotEqual(str, str2);
        }
    }
}