﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ToolkitLibrary.Time;

namespace ToolkitLibrary.Tests
{
    [TestClass]
    public class TimeTest
    {
        [TestMethod]
        public void Round()
        {
            var date = new DateTime(2011, 8, 8, 8, 8, 8);
            Assert.AreEqual(date.Round(new TimeSpan(1, 0, 0)), new DateTime(2011, 8, 8, 8, 0, 0));
        }

        [TestMethod]
        public void Floor()
        {
            var date = new DateTime(2011, 8, 8, 8, 8, 8);
            Assert.AreEqual(date.Floor(new TimeSpan(1, 0, 0)), new DateTime(2011, 8, 8, 8, 0, 0));
        }

        [TestMethod]
        public void Ceil()
        {
            var date = new DateTime(2011, 8, 8, 8, 8, 8);
            Assert.AreEqual(date.Ceil(new TimeSpan(1, 0, 0)), new DateTime(2011, 8, 8, 9, 0, 0));
        }

        [TestMethod]
        public void ToUnixTime()
        {
            var date = new DateTime(2011, 8, 8, 8, 8, 8);
            Console.WriteLine(date.ToUnixTime());
            Assert.IsInstanceOfType(date.ToUnixTime(),typeof(double));
        }
    }
}
